#include <stdio.h>
#include <stdlib.h>
#define MAX 0x100

void titulo(int tabla){ /* parametro formal */
	char titulo[MAX];
	sprintf(titulo, "toilet -fpagga --metal tabla del %i", tabla);
	system(titulo);
}

/* funcion punto de entrada */
int main(){

	/* declaracion de variables */
	int tabla, op1 = 1;
	

        /*menu */	
	printf("¿Que tabla quieres?\n");
	printf("Tabla: ");
	scanf("%i", &tabla);

        titulo(5); /* llamada con parametro 5  */
	

	/* resultados */ 
	printf("%ix%i=%i\n", op1, tabla, op1*tabla);
	op1++;
	printf("%ix%i=%i\n", op1, tabla, op1*tabla);


	return EXIT_SUCCESS;
}
