#include <stdio.h>
#include <stdlib.h>

/* funcion punto de entrada */
int main(){
    unsigned int primo[]={2, 3, 5, 7, 11, 13, 17, 19, 23};
    unsigned  elementos =(unsigned) sizeof(primo) / sizeof(int);
    unsigned *peeping = primo;
    char *tom = (char *) primo;
    printf("PRIMO:\n"
            "=====\n"
            "Localizacion (%p)\n"
            "Elementos: %u [%u..%u]\n"
            "Tamaño: %lu bytes.\n\n",
             primo,
             elementos,
             primo[0], primo[elementos-1],
            sizeof(primo));

      printf("0: %u\n", peeping[0]); 
      printf("1: %u\n", peeping[1]);
      printf("0: %u\n", *peeping); 
      printf("1: %u\n", *(peeping+1));
      printf("Tamaño: %lu bytes.\n", sizeof(peeping));
      printf("\n");

      printf("%X", *(tom+3));
      printf("\n\n");

      	return EXIT_SUCCESS;


}
